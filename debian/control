Source: cuba
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Francesco Montanari <fmnt@fmnt.info>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 11),
               libqt4-dev
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/science-team/cuba
Vcs-Git: https://salsa.debian.org/science-team/cuba.git
Homepage: http://www.feynarts.de/cuba/

Package: libcuba4
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: multidimensional numerical integration -- library package
 The Cuba library offers a choice of four independent routines for
 multidimensional numerical integration: Vegas, Suave, Divonne, and
 Cuhre.  They work by very different methods, first three are Monte
 Carlo based. All four have a C/C++, Fortran interface and can
 integrate vector integrands. Their invocation is very similar, so it
 is easy to substitute one method by another for cross-checking. For
 further safeguarding, the output is supplemented by a chi-square
 probability which quantifies the reliability of the error estimate.
 .
 This package provides the shared libraries required to run programs
 compiled with Cuba. To compile your own programs you also need to
 install libcuba-dev.

Package: libcuba-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libcuba4 (= ${binary:Version}),
         ${misc:Depends}
Suggests: partview
Description: multidimensional numerical integration -- development package
 The Cuba library offers a choice of four independent routines for
 multidimensional numerical integration: Vegas, Suave, Divonne, and
 Cuhre.  They work by very different methods, first three are Monte Carlo
 based. All four have a C/C++, Fortran interface and can integrate vector
 integrands. Their invocation is very similar, so it is easy to substitute
 one method by another for cross-checking. For further safeguarding, the
 output is supplemented by a chi-square probability which quantifies the
 reliability of the error estimate.
 .
 This package contains the header file, static library and symbolic
 links that developers using Cuba will need.  This package also
 contains library documentation and examples.

Package: partview
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: partition viewer for the Cuba library
 Partview reads Cuba verbose output from stdin and displays the
 specified planes of the tessellation on screen.  Each pair of
 dimensions is shown in a separate tab.
 .
 The Cuba library offers a choice of four independent routines for
 multidimensional numerical integration: Vegas, Suave, Divonne, and
 Cuhre.  They work by very different methods, first three are Monte Carlo
 based. All four have a C/C++, Fortran interface and can integrate vector
 integrands. Their invocation is very similar, so it is easy to substitute
 one method by another for cross-checking. For further safeguarding, the
 output is supplemented by a chi-square probability which quantifies the
 reliability of the error estimate.
